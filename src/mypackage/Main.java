package mypackage;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Main {

  public static void main(String[] args) throws IOException {
    // Create a new file
    File logfile = new File("log.txt");
    logfile.createNewFile();

    // Create a new directory
    File dir = new File("logs");
    dir.mkdirs();

    // Move the file into that directory
    File destination = new File(dir, "log.txt");
    logfile.renameTo(destination);

    // Cleanup
    //destination.delete();
    //dir.delete();

    // Important methods in File class
    System.out.println("Absolute Path: " + destination.getAbsoluteFile());
    System.out.println("File Name: " + destination.getName());
    System.out.println("Parent Name: " + destination.getParent());
    System.out.println("File Exists? " + dir.exists());
    System.out.println("Last modified in ms: " + destination.lastModified());

    if (dir.isDirectory()) {
      Arrays.stream(dir.listFiles()).forEach(System.out::println);
    }
  }
}